# CreateVPC

Create a VPC and deploy private and public subnets

## Getting started

Create a VPC and deploy a public and a private subnet 
Then have two EC2 instances deployed in each subnet
Then have public facing EC2 instance connect with EC2 instance in the private subnet
Public facing EC2 will have a web server 
Private subnet will have the database layer

## Steps to follow

- [ ] Create VPC
- [ ] Create Internet Gateway
- [ ] Create custom route table
- [ ] Create subnets
- [ ] Associate subnets to route tables
- [ ] Create security group to allow 22, 44, and 80
- [ ] Create security group to allow 3306 [mySQL] port 
- [ ] Create a network interface with an IP in the subnet
- [ ] Assign an elastic IP to the network interface 
- [ ] Create public facing EC2 instance
- [ ] Create private EC2 instance

