# Describe terraform version and environment 
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
  access_key = "AKIA3RM6GKJ75USUMP3U"
  secret_key = "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"
}

# Step 1: Create a VPC in VPC Only mode
resource "aws_vpc" "vpc-Capstone" {
    cidr_block  = "10.0.0.0/16"
    tags = {
      "Name" = "vpc-Capstone"
    }
}

# Step 2: Create an internet gateway. We will use VPC ID from the vpc name above
resource "aws_internet_gateway" "igw-Capstone" {
    vpc_id = aws_vpc.vpc-Capstone.id
    tags = {
      "Name" = "igw-Capstone"
    }
}

# Step 3: Create a public route table. There is a private route table created by default which is 'main' we need another one
resource "aws_route_table" "rtb-Capstone-pub" {
    vpc_id = aws_vpc.vpc-Capstone.id

    # This is 'Routes' option on the AWS Management console when we assign this route to the internet gateway associated above
    # CIDR block is any from HTTP or HTTPS will be allowed through internet gateway
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.igw-Capstone.id
    }

    route {
        ipv6_cidr_block = "::/0"
        gateway_id      = aws_internet_gateway.igw-Capstone.id
    }

    tags = {
        Name = "rtb-Capstone-pub"
    }
}

# Step 4: Create subnets. One will be public and the other will be private
resource "aws_subnet" "sub-Capstone-Pub" {
    vpc_id = aws_vpc.vpc-Capstone.id
    cidr_block = "10.0.1.0/24"
    availability_zone = "us-east-1a"
    map_public_ip_on_launch = true      # Auto Assign IPV4 public IP address
    tags = {
      "Name" = "sub-Capstone-Pub"
    }
}

resource "aws_subnet" "sub-Capstone-Pri" {
    vpc_id = aws_vpc.vpc-Capstone.id
    cidr_block = "10.0.2.0/24"
    availability_zone = "us-east-1b"
    tags = {
      "Name" = "sub-Capstone-Pri"
    }
}

# Step 5: Associate public facing route table to the public facing subnet. Private subnet will be associated to the private route by default
# Resource name does not matter here but it is a good practice to assign one
resource "aws_route_table_association" "rta-Capstone" {
    subnet_id = aws_subnet.sub-Capstone-Pub.id
    route_table_id = aws_route_table.rtb-Capstone-pub.id
}

# Step 6: Create a security group which will allow ports 22, 80 and 443 to access public facing EC2 instance
resource "aws_security_group" "sgp-Capstone-Pub" {
    name        = "sgp-Capstone-Pub"
    description = "Allow TLS inbound traffic"
    vpc_id      = aws_vpc.vpc-Capstone.id

    # Define all inbound security group rules
    ingress {
        description      = "SSH"
        from_port        = 22
        to_port          = 22
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
    }

    ingress {
        description      = "HTTP"
        from_port        = 80
        to_port          = 80
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
    }    
        
    # ingress {
    #     description      = "HTTPS"
    #     from_port        = 443
    #     to_port          = 443
    #     protocol         = "tcp"
    #     cidr_blocks      = ["0.0.0.0/0"]
    # }

    # Define outbound security group rules. -1 means any protocol
    egress {
        from_port        = 0
        to_port          = 0
        protocol         = "-1"
        cidr_blocks      = ["0.0.0.0/0"]
        # ipv6_cidr_blocks = ["::/0"]
    }

    tags = {
        Name = "sgp-Capstone-Pub"
    }
}

resource "aws_security_group" "sgp-Capstone-Pri" {
    name        = "sgp-Capstone-Pri"
    description = "Allow inbound traffic from inside VPC Only"
    vpc_id      = aws_vpc.vpc-Capstone.id

    # Define all inbound security group rules
    ingress {
        description      = "Custom"
        from_port        = 3306
        to_port          = 3306
        protocol         = "tcp"
        cidr_blocks      = ["10.0.1.0/24"]
    }

    # This is to test ONLY. Once test works, we do not need an inbound 
    ingress {
        description      = "SSH"
        from_port        = 22
        to_port          = 22
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
    }
    # Define outbound security group rules. -1 means any protocol
    egress {
        from_port        = 0
        to_port          = 0
        protocol         = "-1"
        cidr_blocks      = ["0.0.0.0/0"]
    }

    tags = {
        Name = "sgp-Capstone-Pri"
    }
}

# Step 7: Create a public facing instance to run the web server
resource "aws_instance" "ec2-Capstone-Pub" {
    ami = "ami-0022f774911c1d690"
    instance_type = "t2.micro"
    availability_zone = "us-east-1a"
    key_name = "kp_capstone"
    subnet_id = aws_subnet.sub-Capstone-Pub.id
    security_groups = [aws_security_group.sgp-Capstone-Pub.id]
    user_data = <<-EOF
        #!/bin/bash
        sudo su
        yum update -y
        yum install httpd -y
        service httpd start
        chkconfig httpd on
        cd /var/www/html
        echo "<html><h1>Hello Ammar. Your first terraform web application</h1></html>"  >  index.html
    EOF

    tags = {
        "Name" = "ec2-Capstone-Pub"
    }
}

resource "aws_instance" "ec2-Capstone-Pri" {
    ami = "ami-0022f774911c1d690"
    instance_type = "t2.micro"
    availability_zone = "us-east-1b"
    key_name = "kp_capstone"
    subnet_id = aws_subnet.sub-Capstone-Pri.id
    security_groups = [aws_security_group.sgp-Capstone-Pri.id]
    # user_data = <<-EOF
    #     #!/bin/bash
    #     sudo su
    #     yum update -y
    #     yum install mysql
    # EOF

    tags = {
        "Name" = "ec2-Capstone-Pri"
    }
}